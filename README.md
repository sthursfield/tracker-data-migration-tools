# Tracker data migration tools

This repo contains some scripts which can be used to migrate user data from
Tracker 2.x to Tracker 3.x databases.

The scripts require Tracker 3 to be installed. They also require Python and
the Python rdflib package.

Exporting the data will be done using the Tracker 2.x CLI. See
https://gitlab.gnome.org/GNOME/tracker/-/merge_requests/262

## GNOME Photos - Albums

Run the following command:

    tracker export --type=photos-albums | ./photos-import-albums

## GNOME Photos - Favorite Photos

Run the following command:

    tracker export --type=photos-favorites | ./photos-import-favorites

## Nautilus - Starred Files

Run the following command:

    tracker export --type=files-starred | ./nautilus-import-starred-files
